package webuy

import (
	"encoding/json"
	"net/http"
)

type StockListStocksRequest struct {
	SkuIds []int64 `json:"skuIds"`
}

type StockListStocksResponse struct {
	BaseResponse
	Entry []struct {
		Barcode string `json:"barcode"`
		SkuId   int64  `json:"skuId"`
		Stock   int    `json:"stock"`
	} `json:"entry"`
}

func (req *StockListStocksRequest) ApiPath() string {
	return "/open/api/stock/listStocks"
}

func (req *StockListStocksRequest) RequestMethod() string {
	return http.MethodPost
}

func (resp *StockListStocksResponse) String() string {
	buf, err := json.Marshal(resp)
	if err != nil {
		panic(buf)
	}
	return string(buf)
}

func (client *Client) StockListStocks(req *StockListStocksRequest) (resp *StockListStocksResponse, err error) {
	resp = &StockListStocksResponse{}
	err = client.Do(req, resp)
	return
}
