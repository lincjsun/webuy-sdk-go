package webuy

import (
	"net/http"
	"testing"
)

func TestStockBatchUpdateStockRequestApiPath(t *testing.T) {
	req := &StockBatchUpdateStockRequest{}
	apiPath := req.ApiPath()
	expect := "/open/api/stock/batchUpdateStock"
	if apiPath != expect {
		t.Fatalf("expect %s but actual %s", expect, apiPath)
	}
}

func TestBatchUpdateStockRequestRequestMethod(t *testing.T) {
	req := &StockBatchUpdateStockRequest{}
	method := req.RequestMethod()
	if method != http.MethodPost {
		t.Fatalf("expect %s but actual %s", http.MethodPost, method)
	}
}

func TestStockBatchUpdateStockResponseString(t *testing.T) {
	resp := &StockBatchUpdateStockResponse{}
	s := resp.String()
	expect := `{"status":false}`
	if s != expect {
		t.Fatalf("expect %s but actual %s", expect, s)
	}
}

func TestStockBatchUpdateStock(t *testing.T) {
	client := NewClient(
		WithUrl(UrlTesting),
		WithAppId(appId),
		WithAppSecret(appSecret),
		WithSupplierDockId(supplierDockId),
	)
	req := &StockBatchUpdateStockRequest{}
	resp, err := client.StockBatchUpdateStock(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.Status == false {
		t.Fatalf("%+v", resp)
	}
}
