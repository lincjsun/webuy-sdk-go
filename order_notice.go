package webuy

import "net/http"

type OrderNoticeRequest struct {
	BaseRequest
	Orders []struct {
		ParentOrderId  string `json:"parentOrderId"`
		OrderId        int64  `json:"orderId"`
		ReceiverName   string `json:"receiverName"`
		Mobile         string `json:"mobile"`
		ProvinceName   string `json:"provinceName"`
		CityName       string `json:"cityName"`
		AreaName       string `json:"areaName"`
		PartAddress    string `json:"partAddress"`
		GmtSubmitOrder string `json:"gmtSubmitOrder"`
		GmtModifyOrder string `json:"gmtModifyOrder"`
		CutOffId       int64  `json:"cutOffId"`
		GmtCutOff      string `json:"gmtCutOff"`
		Status         int    `json:"status"`
		OrderRemark    string `json:"orderRemark"`
		PushReason     int    `json:"pushReason"`
		OrderDetails   []struct {
			SkuId                   int64  `json:"skuId"`
			ItemName                string `json:"itemName"`
			Barcode                 string `json:"barcode"`
			SpuCode                 string `json:"spuCode"`
			ItemNum                 int    `json:"itemNum"`
			ItemPrice               int64  `json:"itemPrice"`
			ItemMainAttributes      string `json:"itemMainAttributes"`
			ItemSecondAttributes    string `json:"itemSecondAttributes"`
			TotalPrice              int64  `json:"totalPrice"`
			SupplierInsuranceAmount int64  `json:"supplierInsuranceAmount"`
			OrderPostageInfo        struct {
				Postage float64 `json:"postage"`
				Type    int     `json:"type"`
			} `json:"orderPostageInfo,omitempty"`
			OrderSupplierActivityInfos []struct {
				ActivityId      int64  `json:"activityId"`
				ActivityName    string `json:"activityName"`
				UnderTakeAmount int64  `json:"underTakeAmount"`
			} `json:"orderSupplierActivityInfos,omitempty"`
			OrderSupplierCouponInfos []struct {
				CoupondId       int64  `json:"coupondId"`
				UnderTakeAmount int64  `json:"underTakeAmount"`
				CouponName      string `json:"couponName"`
			} `json:"orderSupplierCouponInfos,omitempty"`
		} `json:"orderDetails"`
	} `json:"orders"`
}

type OrderNoticeResponse struct {
	BaseResponse
}

func (req *OrderNoticeRequest) RequestMethod() string {
	return http.MethodPost
}
