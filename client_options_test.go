package webuy

import "testing"

func TestNewOptions(t *testing.T) {
	options := NewOptions(
		WithUrl(UrlTesting),
		WithAppId("appId"),
		WithAppSecret("appSecret"),
		WithSupplierDockId("supplierDockId"),
	)
	if options.Url != UrlTesting {
		t.FailNow()
	}
	if options.AppId != "appId" {
		t.FailNow()
	}
	if options.AppSecret != "appSecret" {
		t.FailNow()
	}
	if options.SupplierDockId != "supplierDockId" {
		t.FailNow()
	}
}
