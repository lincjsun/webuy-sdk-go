package webuy

import (
	"net/http"
	"testing"
)

func TestOrderNoticeRequestMethod(t *testing.T) {
	req := &OrderNoticeRequest{}
	method := req.RequestMethod()
	if method != http.MethodPost {
		t.Fatalf("expect %s but actual %s", http.MethodPost, method)
	}
}
