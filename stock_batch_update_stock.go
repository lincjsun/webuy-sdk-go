package webuy

import (
	"encoding/json"
	"net/http"
)

type StockBatchUpdateStockRequest struct {
	Updates []struct {
		SkuId int64 `json:"skuId"`
		Stock int   `json:"stock"`
	} `json:"updates"`
}

type StockBatchUpdateStockResponse struct {
	BaseResponse
}

func (req *StockBatchUpdateStockRequest) ApiPath() string {
	return "/open/api/stock/batchUpdateStock"
}

func (req *StockBatchUpdateStockRequest) RequestMethod() string {
	return http.MethodPost
}

func (resp *StockBatchUpdateStockResponse) String() string {
	buf, err := json.Marshal(resp)
	if err != nil {
		panic(err)
	}
	return string(buf)
}

func (client *Client) StockBatchUpdateStock(req *StockBatchUpdateStockRequest) (resp *StockBatchUpdateStockResponse, err error) {
	resp = &StockBatchUpdateStockResponse{}
	err = client.Do(req, resp)
	return
}
