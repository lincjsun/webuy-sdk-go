package webuy

import (
	"encoding/json"
	"net/http"
)

type DeliveryNoticeRequest struct {
	OrderId         int64  `json:"orderId"`
	LogisticCompany string `json:"logisticCompany"`
	LogisticCode    string `json:"logisticCode"`
	DeliveryDetails []struct {
		SkuId   int64 `json:"skuId"`
		ItemNum int   `json:"itemNum"`
	} `json:"deliveryDetails"`
}

type DeliveryNoticeResponse struct {
	BaseResponse
}

func (req *DeliveryNoticeRequest) ApiPath() string {
	return "/open/api/delivery/notice"
}
func (req *DeliveryNoticeRequest) RequestMethod() string {
	return http.MethodPost
}

func (resp *DeliveryNoticeResponse) String() string {
	buf, err := json.Marshal(resp)
	if err != nil {
		panic(err)
	}
	return string(buf)
}

func (client *Client) DeliveryNotice(req *DeliveryNoticeRequest) (resp *DeliveryNoticeResponse, err error) {
	resp = &DeliveryNoticeResponse{}
	err = client.Do(req, resp)
	return
}
