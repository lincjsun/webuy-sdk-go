package webuy

import (
	"testing"
)

func TestOrderListOrderIdsResponseString(t *testing.T) {
	resp := &OrderListOrderIdsResponse{}
	expect := `{"status":false}`
	actual := resp.String()
	if expect != actual {
		t.Fatalf("expect %s but actual %s", expect, actual)
	}
}

func TestOrderListOrderIds(t *testing.T) {
	client := NewClient(
		WithUrl(UrlProduction),
		WithAppId(appId),
		WithAppSecret(appSecret),
		WithSupplierDockId(supplierDockId),
	)
	req := &OrderListOrderIdsRequest{
		Status:    []int{1, 2, 3, 4, 5},
		GmtStart:  "2019-05-21 11:49:18",
		GmtEnd:    "2019-05-22 11:49:18",
		FetchType: 0,
		PageNo:    1,
		PageSize:  3,
	}
	resp, err := client.OrderListOrderIds(req)
	if err != nil {
		t.Fatal("OrderListOrderIds error:", err)
	}
	if resp.Status == false {
		t.Fatal("OrderListOrderIds response:", resp.String())
	}
}
