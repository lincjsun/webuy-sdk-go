package webuy

import "testing"

func TestMd5Sum(t *testing.T) {
	actual := md5Sum("123456")
	expect := "e10adc3949ba59abbe56e057f20f883e"
	if actual != expect {
		t.Fatalf("expect %s but actual %s", expect, actual)
	}
}

func TestSign(t *testing.T) {
	m := make(map[string]interface{})
	s := Sign(m, "")

	m["appId"] = "11111"
	m["gmtRequest"] = "1560237616188"
	m["randomStr"] = "e616d1a8-28ba-428a-8f35-b4f98adabb17"
	m["supplierDockId"] = "33333"
	s = Sign(m, "密钥")
	expect := "D0CE9F42BAC4778C646FD339302B6A04"
	if s != expect {
		t.Fatalf("expect %s but actual %s", expect, s)
	}
}
