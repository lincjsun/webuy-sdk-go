package webuy

import "testing"

func TestOrderListOrdersResponseString(t *testing.T){
	resp := &OrderListOrdersResponse{}
	s := resp.String()
	expect := `{"status":false}`
	if s != expect {
		t.Fatalf("expect %s but actual %s", expect, s)
	}
}

func TestOrderListOrders(t *testing.T) {
	client := NewClient(
		WithUrl(UrlProduction),
		WithAppId(appId),
		WithAppSecret(appSecret),
		WithSupplierDockId(supplierDockId),
	)
	req := &OrderListOrdersRequest{}
	resp, err := client.OrderListOrders(req)
	if err != nil{
		t.Fatal(err)
	}
	if resp.Status == false{
		t.Fatalf("%+v", resp)
	}
}
