package webuy

import (
	"net/http"
	"testing"
)

func TestStockListStocksRequestApiPath(t *testing.T) {
	req := &StockListStocksRequest{}
	s := req.ApiPath()
	expect := "/open/api/stock/listStocks"
	if s != expect {
		t.Fatalf("expect %s but actual %s", expect, s)
	}
}

func TestStockListStocksRequestRequestMethod(t *testing.T) {
	req := &StockListStocksRequest{}
	method := req.RequestMethod()
	if method != http.MethodPost {
		t.Fatalf("expect %s but actual %s", http.MethodPost, method)
	}
}

func TestStockListStocksResponseString(t *testing.T) {
	resp := &StockListStocksResponse{}
	s := resp.String()
	expect := `{"status":false,"entry":null}`
	if s != expect {
		t.Fatalf("expect %s but actual %s", expect, s)
	}
}

func TestStockListStocks(t *testing.T) {
	client := NewClient(
		WithUrl(UrlProduction),
		WithAppId(appId),
		WithAppSecret(appSecret),
		WithSupplierDockId(supplierDockId),
	)
	req := &StockListStocksRequest{
		SkuIds: []int64{123456},
	}
	resp, err := client.StockListStocks(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.Status == false {
		t.Fatalf("%+v", resp)
	}
}
