package webuy

type Options struct {
	Url            string
	AppId          string
	AppSecret      string
	SupplierDockId string
}

type Option func(*Options)

func NewOptions(options ...Option) Options {
	opts := Options{
		Url:            "",
		AppId:          "",
		AppSecret:      "",
		SupplierDockId: "",
	}
	for _, o := range options {
		o(&opts)
	}
	return opts
}

// WithUrl
func WithUrl(url string) Option {
	return func(o *Options) {
		o.Url = url
	}
}

// WithAppId
func WithAppId(appId string) Option {
	return func(o *Options) {
		o.AppId = appId
	}
}

// WithAppSecret
func WithAppSecret(appSecret string) Option {
	return func(o *Options) {
		o.AppSecret = appSecret
	}
}

// WithSupplierDockId
func WithSupplierDockId(supplierDockId string) Option {
	return func(o *Options) {
		o.SupplierDockId = supplierDockId
	}
}
