package webuy

import (
	"encoding/json"
	"net/http"
)

type OrderListOrderIdsRequest struct {
	Status    []int  `json:"status"`
	GmtStart  string `json:"gmtStart"`
	GmtEnd    string `json:"gmtEnd"`
	FetchType int    `json:"fetchType"`
	PageNo    int    `json:"pageNo"`
	PageSize  int    `json:"pageSize"`
}

type OrderListOrderIdsResponse struct {
	BaseResponse
	//Entry []int `json:"entry"`
}

func (r *OrderListOrderIdsRequest) ApiPath() string {
	return "/open/api/order/listOrderIds"
}

func (r *OrderListOrderIdsRequest) RequestMethod() string {
	return http.MethodPost
}

func (resp *OrderListOrderIdsResponse) String() string {
	buf, err := json.Marshal(resp)
	if err != nil {
		panic(err)
	}
	return string(buf)
}

func (client *Client) OrderListOrderIds(req *OrderListOrderIdsRequest) (resp *OrderListOrderIdsResponse, err error) {
	resp = &OrderListOrderIdsResponse{}
	err = client.Do(req, resp)
	return
}
