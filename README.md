# webuy-sdk-go

鲸灵/好衣库/甩甩宝宝软件开发工具包(Go)

## 相关

- [鲸灵商家后台](http://scrm.webuy.ai/login)
- [鲸灵开放平台接入规范](https://www.showdoc.com.cn/p/113bb5c3664c3411bd5b0c35f1560465)
- [鲸灵SCRM对接第三方资源管理系统使用说明](https://shimo.im/docs/vRyWTvJhY9rPvQVt/read)