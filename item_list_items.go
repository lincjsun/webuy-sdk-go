package webuy

import (
	"encoding/json"
	"net/http"
)

type ItemListItemsRequest struct {
	PageNo   int `json:"pageNo,omitempty"`
	PageSize int `json:"pageSize,omitempty"`
}

type ItemListItemsResponse struct {
	BaseResponse
	Entry []struct {
		SpuId           int64  `json:"spuId"`
		SupplierSpuCode string `json:"supplierSpuCode"`
		Name            string `json:"name"`
		ShortName       string `json:"shortName"`
		Status          int    `json:"status"`
		SkuInfo         []struct {
			SkuId           int64    `json:"skuId"`
			Name            string   `json:"name"`
			Barcode         string   `json:"barcode"`
			HeadPictures    []string `json:"headPictures"`
			TerminalPrice   int64    `json:"terminalPrice"`
			MainAttribute   string   `json:"mainAttribute"`
			SecondAttribute string   `json:"secondAttribute"`
		} `json:"skuInfo"`
	} `json:"entry"`
}

func (req *ItemListItemsRequest) ApiPath() string {
	return "/open/api/item/listItems"
}

func (req *ItemListItemsRequest) RequestMethod() string {
	return http.MethodPost
}

func (resp *ItemListItemsResponse) String() string {
	buf, err := json.Marshal(resp)
	if err != nil {
		panic(err)
	}
	return string(buf)
}

func (client *Client) ItemListItems(req *ItemListItemsRequest) (resp *ItemListItemsResponse, err error) {
	resp = &ItemListItemsResponse{}
	err = client.Do(req, resp)
	return
}
