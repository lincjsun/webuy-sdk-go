package webuy

import (
	"net/http"
	"testing"
)

func TestDeliveryNoticeRequestApiPath(t *testing.T) {
	req := &DeliveryNoticeRequest{}
	apiPath := req.ApiPath()
	expect := "/open/api/delivery/notice"
	if apiPath != expect {
		t.Fatalf("expect %s but actual %s", expect, apiPath)
	}
}

func TestDeliveryNoticeRequestRequestMethod(t *testing.T) {
	req := &DeliveryNoticeRequest{}
	method := req.RequestMethod()
	if method != http.MethodPost {
		t.Fatalf("expect %s but actual %s", http.MethodPost, method)
	}
}

func TestDeliveryNoticeResponseString(t *testing.T) {
	resp := &DeliveryNoticeResponse{}
	s := resp.String()
	expect := `{"status":false}`
	if s != expect {
		t.Fatalf("expect %s but actual %s", expect, s)
	}
}

func TestDeliveryNotice(t *testing.T) {
	client := NewClient(
		WithUrl(UrlTesting),
		WithAppId(appId),
		WithAppSecret(appSecret),
		WithSupplierDockId(supplierDockId),
	)
	req := &DeliveryNoticeRequest{}
	resp, err := client.DeliveryNotice(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.Status == false {
		t.Fatalf("%+v", resp)
	}
}
