package webuy

import (
	"encoding/json"
	"net/http"
)

type OrderListOrdersRequest struct {
	OrderIds []int64 `json:"orderIds"`
}

type OrderListOrdersResponse struct {
	BaseResponse
}

func (req *OrderListOrdersRequest) ApiPath() string {
	return "/open/api/order/listOrders"
}

func (req *OrderListOrdersRequest) RequestMethod() string {
	return http.MethodPost
}

func (resp *OrderListOrdersResponse) String() string {
	buf, err := json.Marshal(resp)
	if err != nil{
		panic(err)
	}
	return string(buf)
}

func (client *Client) OrderListOrders(req *OrderListOrdersRequest) (resp *OrderListOrdersResponse, err error) {
	resp = &OrderListOrdersResponse{}
	err = client.Do(req, resp)
	return
}
