package webuy

import "testing"

func TestItemListItemsResponseString(t *testing.T) {
	resp := &ItemListItemsResponse{}
	s := resp.String()
	expect := `{"status":false,"entry":null}`
	if s != expect {
		t.Fatalf("expect %s but actual %s", expect, s)
	}
}

func TestItemListItems(t *testing.T) {
	client := NewClient(
		WithUrl(UrlProduction),
		WithAppId(appId),
		WithAppSecret(appSecret),
		WithSupplierDockId(supplierDockId),
	)
	req := &ItemListItemsRequest{
		PageNo:   1,
		PageSize: 2,
	}
	resp, err := client.ItemListItems(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.Status == false {
		t.Fatalf("%+v", resp)
	}
}
