package webuy

import (
	"flag"
	"fmt"
	"testing"
)

var (
	appId          string
	appSecret      string
	supplierDockId string
)

func TestMain(m *testing.M) {
	flag.StringVar(&appId, "appId", "", "appId")
	flag.StringVar(&appSecret, "appSecret", "", "appSecret")
	flag.StringVar(&supplierDockId, "supplierDockId", "", "supplierDockId")
	if !flag.Parsed() {
		flag.Parse()
	}
	fmt.Printf("-args -appId %s -appSecret %s -supplierDockId %s\n", appId, appSecret, supplierDockId)
	m.Run()
}
